package com.tistory.blackrug.rc_project;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends Activity
        implements SensorEventListener {
    static final int REQUEST_ENABLE_BT = 10;
    Sensor accelerometer;
    boolean angleFlag = false;
    int data = 0;
    int[] intTemp = new int[3];
    BluetoothAdapter mBluetoothAdapter;
    char mCharDelimiter = '\n';
    Set<BluetoothDevice> mDevices;
    Button mEdit0;
    Button mEdit45;
    Button mEdit90;
    ToggleButton mEditBack;
    ToggleButton mEditForward;
    SeekBar mEditSeek;
    TextView mEditSpeed;
    Button mEdit_45;
    Button mEdit_90;
    float[] mGeomagnetic;
    float[] mGravity;
    InputStream mInputStream = null;
    OutputStream mOutputStream = null;
    int mPairedDeviceCount = 0;
    BluetoothDevice mRemoteDevice;
    private SensorManager mSensorManager;
    BluetoothSocket mSocket = null;
    String mStrDelimiter = "\n";
    Thread mWorkerThread = null;
    Sensor magnetometer;
    float pitch;
    byte[] readBuffer;
    int readBufferPosition;
    String[] temp;

    void checkBluetooth() {
        this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (this.mBluetoothAdapter == null) {
            Toast.makeText(getApplicationContext(), "기기가 블루투스를 지원하지 않습니다.", 1).show();
            finish();
            return;
        }
        if (!this.mBluetoothAdapter.isEnabled()) {
            Toast.makeText(getApplicationContext(), "현재 블루투스가 비활성 상태입니다.", 1).show();
            startActivityForResult(new Intent("android.bluetooth.adapter.action.REQUEST_ENABLE"), 10);
            return;
        }
        selectDevice();
    }

    void connectToSelectedDevice(String paramString) {
        this.mRemoteDevice = getDeviceFromBondedList(paramString);
        paramString = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
        try {
            this.mSocket = this.mRemoteDevice.createRfcommSocketToServiceRecord(paramString);
            this.mSocket.connect();
            this.mOutputStream = this.mSocket.getOutputStream();
            this.mInputStream = this.mSocket.getInputStream();
            sendData("t");
            return;
        } catch (Exception paramString) {
            Toast.makeText(getApplicationContext(), "블루투스 연결 중 오류가 발생했습니다.", 1).show();
            finish();
        }
    }

    BluetoothDevice getDeviceFromBondedList(String paramString) {
        Iterator localIterator = this.mDevices.iterator();
        BluetoothDevice localBluetoothDevice;
        do {
            if (!localIterator.hasNext())
                return null;
            localBluetoothDevice = (BluetoothDevice) localIterator.next();
        }
        while (!paramString.equals(localBluetoothDevice.getName()));
        return localBluetoothDevice;
    }

    public void onAccuracyChanged(Sensor paramSensor, int paramInt) {
    }

    protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent) {
        switch (paramInt1) {
            default:
            case 10:
        }
        while (true) {
            super.onActivityResult(paramInt1, paramInt2, paramIntent);
            return;
            if (paramInt2 == -1) {
                selectDevice();
                continue;
            }
            if (paramInt2 != 0)
                continue;
            Toast.makeText(getApplicationContext(), "블루투스를 사용할 수 없어 프로그램을 종료합니다.", 1).show();
            finish();
        }
    }

    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(2130903064);
        this.mSensorManager = ((SensorManager) getSystemService("sensor"));
        this.accelerometer = this.mSensorManager.getDefaultSensor(1);
        this.magnetometer = this.mSensorManager.getDefaultSensor(2);
        this.mEditForward = ((ToggleButton) findViewById(2131034180));
        this.mEditBack = ((ToggleButton) findViewById(2131034179));
        this.mEdit_90 = ((Button) findViewById(2131034176));
        this.mEdit_45 = ((Button) findViewById(2131034173));
        this.mEdit0 = ((Button) findViewById(2131034174));
        this.mEdit45 = ((Button) findViewById(2131034172));
        this.mEdit90 = ((Button) findViewById(2131034175));
        this.mEditSpeed = ((TextView) findViewById(2131034177));
        this.mEditSeek = ((SeekBar) findViewById(2131034178));
        this.mEditForward.setOnClickListener(new View.OnClickListener() {
            public void onClick(View paramView) {
                if (MainActivity.this.mEditForward.isChecked()) {
                    MainActivity.this.sendData("F");
                    MainActivity.this.mEditBack.setChecked(false);
                    MainActivity.this.angleFlag = true;
                    return;
                }
                MainActivity.this.sendData("f");
                MainActivity.this.mEditSeek.setProgress(0);
                MainActivity.this.angleFlag = false;
            }
        });
        this.mEditBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View paramView) {
                if (MainActivity.this.mEditBack.isChecked()) {
                    MainActivity.this.sendData("B");
                    MainActivity.this.mEditForward.setChecked(false);
                    MainActivity.this.angleFlag = true;
                    return;
                }
                MainActivity.this.sendData("b");
                MainActivity.this.mEditSeek.setProgress(0);
                MainActivity.this.angleFlag = false;
            }
        });
        this.mEdit_90.setOnClickListener(new View.OnClickListener() {
            public void onClick(View paramView) {
                MainActivity.this.sendData("j");
            }
        });
        this.mEdit_45.setOnClickListener(new View.OnClickListener() {
            public void onClick(View paramView) {
                MainActivity.this.sendData("i");
            }
        });
        this.mEdit0.setOnClickListener(new View.OnClickListener() {
            public void onClick(View paramView) {
                MainActivity.this.sendData("k");
            }
        });
        this.mEdit45.setOnClickListener(new View.OnClickListener() {
            public void onClick(View paramView) {
                MainActivity.this.sendData("o");
            }
        });
        this.mEdit90.setOnClickListener(new View.OnClickListener() {
            public void onClick(View paramView) {
                MainActivity.this.sendData("l");
            }
        });
        this.mEditSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar paramSeekBar, int paramInt, boolean paramBoolean) {
                MainActivity.this.mEditSpeed.setText("Speed : " + paramInt);
                MainActivity.this.sendData(paramInt + "q");
            }

            public void onStartTrackingTouch(SeekBar paramSeekBar) {
            }

            public void onStopTrackingTouch(SeekBar paramSeekBar) {
            }
        });
        checkBluetooth();
    }

    public boolean onCreateOptionsMenu(Menu paramMenu) {
        getMenuInflater().inflate(2131492864, paramMenu);
        return true;
    }

    protected void onDestroy() {
        try {
            this.mWorkerThread.interrupt();
            this.mInputStream.close();
            this.mOutputStream.close();
            this.mSocket.close();
            label28:
            super.onDestroy();
            return;
        } catch (Exception localException) {
            break label28;
        }
    }

    protected void onPause() {
        super.onPause();
        this.mSensorManager.unregisterListener(this);
    }

    protected void onResume() {
        super.onResume();
        this.mSensorManager.registerListener(this, this.accelerometer, 3);
        this.mSensorManager.registerListener(this, this.magnetometer, 3);
    }

    public void onSensorChanged(SensorEvent paramSensorEvent) {
        if (paramSensorEvent.sensor.getType() == 1)
            this.mGravity = paramSensorEvent.values;
        if (paramSensorEvent.sensor.getType() == 2)
            this.mGeomagnetic = paramSensorEvent.values;
        if ((this.mGravity != null) && (this.mGeomagnetic != null)) {
            paramSensorEvent = new float[9];
            if (SensorManager.getRotationMatrix(paramSensorEvent, new float[9], this.mGravity, this.mGeomagnetic)) {
                float[] arrayOfFloat = new float[3];
                SensorManager.getOrientation(paramSensorEvent, arrayOfFloat);
                this.pitch = arrayOfFloat[1];
                this.data = (int) (this.pitch * 100.0F);
                if ((!this.angleFlag) || (this.data < 0))
                    break label148;
                sendData(this.data + "w");
            }
        }
        label148:
        do
            return;
        while ((!this.angleFlag) || (this.data >= 0));
        sendData(Math.abs(this.data) + "e");
    }

    void selectDevice() {
        this.mDevices = this.mBluetoothAdapter.getBondedDevices();
        this.mPairedDeviceCount = this.mDevices.size();
        if (this.mPairedDeviceCount == 0) {
            Toast.makeText(getApplicationContext(), "페어링된 장치가 없습니다.", 1).show();
            finish();
        }
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setTitle("블루투스 장치 선택");
        Object localObject = new ArrayList();
        Iterator localIterator = this.mDevices.iterator();
        while (true) {
            if (!localIterator.hasNext()) {
                ((List) localObject).add("취소");
                localObject = (CharSequence[]) ((List) localObject).toArray(new CharSequence[((List) localObject).size()]);
                localBuilder.setItems(localObject, new DialogInterface.OnClickListener(localObject) {
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        if (paramInt == MainActivity.this.mPairedDeviceCount) {
                            Toast.makeText(MainActivity.this.getApplicationContext(), "연결할 장치를 선택하지 않았습니다.", 1).show();
                            MainActivity.this.finish();
                            return;
                        }
                        MainActivity.this.connectToSelectedDevice(this.val$items[paramInt].toString());
                    }
                });
                localBuilder.setCancelable(false);
                localBuilder.create().show();
                return;
            }
            ((List) localObject).add(((BluetoothDevice) localIterator.next()).getName());
        }
    }

    void sendData(String paramString) {
        paramString = paramString + this.mStrDelimiter;
        try {
            this.mOutputStream.write(paramString.getBytes());
            return;
        } catch (Exception paramString) {
            Toast.makeText(getApplicationContext(), "데이터 전송 중 오류가 발생했습니다.", 1).show();
            finish();
        }
    }
}